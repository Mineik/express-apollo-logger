/*
	Code in this file is licensed under MIT License.
	Full license is located in LICENSE file in project root
*/

import {logGQLReq} from "./logger";

export const loggerApolloPlugin ={
	requestDidStart(){
		let success = true
		return{
			didEncounterErrors(){
				success = false
			},
			willSendResponse(){
				logGQLReq(success)
			}
		}
	}
}