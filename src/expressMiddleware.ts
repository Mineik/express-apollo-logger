/*
	Code in this file is licensed under MIT License.
	Full license is located in LICENSE file in project root
*/

import {logHTTPReq} from "./logger";

export function loggerMiddleware(req,res, next){
	logHTTPReq(req, res.statusCode)
	if(!req.baseUrl.startsWith("/")) next()
}