# Logger

## How to use
* Install: `npm install express-apollo-logger express` or yarn add `express-apollo-logger express`
* If you want to use debug only logs make sure to add `MODE=debug` to your `.env` file
* Add into your code:
``` javascript
import {logger, loggerMiddleware} from "express-apollo-logger" //using ES6 imports

/* Supported log types */
//you can import {logTypes} from "express-apollo-logger" if you wish to use enums in typescript
logger(0, "Hello world") //or logTypes.INFO if you use typescript
logger(1, "Debug log") //or logTypes.DEBUG
logger(2, "Error") //or logTypes.ERROR
logger(3, "Warning") //or logTypes.WARN
logger(4, "HTTP request") //or logTypes.HTTP
logger(5, "GraphQL request") // or logTypes.GQL


/* Logging HTTP request manually */
const express = require("express")
const app = express()
import {logHTTPReq} from "express-apollo-logger" 

app.get("/logMe", (request, response)=>{
    //do something
    logHTTPReq(req, /*Your status code*/ 200) // this will log method, path of request and status code
})


/* Logging HTTP request automatically*/
app.get("/autoLog", (request, response, next)=>{
    //... do something
    next() //allow app to continue to middleware
})

app.use(loggerMiddleware) //use supplied middleware to handle all of the logging by itself

```

## Types
Types are supplied with the package